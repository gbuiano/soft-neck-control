#include "Cia402device.h"
#include "CiA301CommPort.h"
#include "SocketCanPort.h"
#include <iostream>


int main ()
{

    //--Can port communications--
    SocketCanPort pm31("can0");
    SocketCanPort pm32("can0");
    SocketCanPort pm33("can0");

    CiA402SetupData sd31(2048,24,0.001, 0.144,20);
    CiA402SetupData sd32(2048,24,0.001, 0.144,20);
    CiA402SetupData sd33(2048,24,0.001, 0.144,20);

    CiA402Device m31 (1, &pm31, &sd31);
    CiA402Device m32 (2, &pm32, &sd32);
    CiA402Device m33 (3, &pm33, &sd33);


//    Motor setup
     m31.Reset();
     m31.SwitchOn();

     m32.Reset();
     m32.SwitchOn();

     m33.Reset();
     m33.SwitchOn();
//     m33.DisablePDOs();


    //set velocity and aceleration (rads/s, rads/s^2)
     m31.SetupPositionMode();
     m32.SetupPositionMode();
     m33.SetupPositionMode();

     cout << "Motors Started. " << endl;
     cout << "m31: " << m31.GetPosition() <<"m32: " << m32.GetPosition() <<"m33: " << m33.GetPosition() << endl;


     m31.SetPosition(0);
     m32.SetPosition(0);
     m33.SetPosition(5);

     sleep(5);

}
