#include "Cia402device.h"
#include "CiA301CommPort.h"
#include "SocketCanPort.h"
#include <iostream>

#include "fcontrol.h"


int main ()
{
    //--Can port communications--
    SocketCanPort pm31("can0");
    SocketCanPort pm32("can0");
    SocketCanPort pm33("can0");

    CiA402SetupData sd1(2048,24,0.001, 0.144,20);

    CiA402Device m1 (1, &pm31, &sd1);
    CiA402Device m2 (2, &pm32, &sd1);
    CiA402Device m3 (3, &pm33, &sd1);

    double dts=0.001;
    double v;

    SamplingTime tools;
    tools.SetSamplingTime(dts);


    //    Motor setup
    m1.Reset();
    m1.SwitchOn();
    m1.Setup_Torque_Mode();
    m2.Reset();
    m2.SwitchOn();
    m2.Setup_Torque_Mode();
    m3.Reset();
    m3.SwitchOn();
    m3.Setup_Torque_Mode();
    //m31.Setup_Velocity_Mode(0,1);

    m1.SetTorque(0.15);
    m2.SetTorque(0.15);
    m3.SetTorque(0.15);
    //m31.SetVelocity(5);

    for(double t=0; t<2; t+=dts){
        v= m1.GetVelocity();
        cout <<"vel: "<< v <<endl;
        tools.WaitSamplingTime();
    }


    sleep(1);
    m1.Setup_Velocity_Mode(0,1);
    m1.SetVelocity(0.0);
    m2.Setup_Velocity_Mode(0,1);
    m2.SetVelocity(0.0);
    m3.Setup_Velocity_Mode(0,1);
    m3.SetVelocity(0.0);

}
